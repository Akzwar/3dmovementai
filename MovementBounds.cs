﻿using UnityEngine;

public class MovementBounds : MovementAffector
{
    public Bounds Bounds;

    Vector3 target = Vector3.zero;

    private void Update()
    {
        if( !Bounds.Contains( transform.position ) )
        {
            if( target == Vector3.zero )
                target = Vector3.Scale( Random.insideUnitSphere * 0.5f, Bounds.size ) + Bounds.center;
            velocity = target - transform.position;
        }
        else
        {
            velocity = Vector3.zero;
            target = Vector3.zero;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube( Bounds.center, Bounds.size );
    }
}
