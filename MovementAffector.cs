﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof( BasicMovement ))]
public abstract class MovementAffector : MonoBehaviour
{
    public string Name;
    public float Weight = 1;
    protected BasicMovement movement => GetComponent<BasicMovement>();

    protected Vector3 velocity;

    public virtual Vector3 GetVelocity()
    {
        return velocity.normalized * Weight;
    }
}
