﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof( BasicMovement ))]
public class SeekMultiple : MovementAffector
{
    public List<Transform> Targets;

    private void Update()
    {
        Targets.RemoveAll( t => t == null );
        velocity = Vector3.zero;
        foreach( Transform t in Targets )
            velocity += t.position - transform.position;
        velocity = velocity / Targets.Count;
    }
}
