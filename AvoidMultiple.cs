﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AvoidMultiple : MovementAffector
{
    public List<Transform> Targets;
    public float Distance = 1;

    private void Update()
    {
        Targets.RemoveAll( t => t == null );
        velocity = Vector3.zero;
        int i = 0;
        foreach( Transform targ in Targets )
        {
            float dist = Vector3.Distance( targ.position, transform.position );
            if( dist < Distance )
            {
                velocity += transform.position - targ.position;
                i++;
            }
        }

        if( i != 0 )
            velocity = velocity / i;
    }
}

