﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Avoid : MovementAffector
{
    public Transform Target;
    public float Distance = 1;

    private void Update()
    {
        float dist = Vector3.Distance( Target.position, transform.position );
        if( dist < Distance )
            velocity = transform.position - Target.position;
        else
            velocity = Vector3.zero;
    }
}
