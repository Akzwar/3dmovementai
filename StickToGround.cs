﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StickToGround : MonoBehaviour
{
    public TerrainCollider Ground;
    public float offset;
    public bool StickRotation = true;
    public bool StickBones = true;
    public int BonesSmoothing = 10;
    public Transform RootBone;
    public List<Queue<float>> bone_position_queues;

    private List<Transform> bones;
    private List<Vector3> bone_positions;

    private List<Transform> GetChildBonesExpanded( Transform bone )
    {
        List<Transform> result = new List<Transform>();
        foreach( Transform child in bone )
        {
            result.Add( child );
            result.AddRange( GetChildBonesExpanded( child ) );
        }

        return result;
    }

    private void Start()
    {
        bones = GetChildBonesExpanded( RootBone );
        bone_positions = bones.Select( b => b.transform.localPosition ).ToList();
        bone_position_queues = bones.Select( b => new Queue<float>() ).ToList();
    }

    void Update ()
    {
        RaycastHit hit;

        Ray down_ray = new Ray( transform.position, Vector3.down );

        if( Ground.Raycast( down_ray, out hit, 100 ) )
        {
            Vector3 pos = transform.position;
            pos.y = hit.point.y + offset;
            transform.position = pos;
            if( StickRotation )
                transform.LookAt( transform.position + hit.normal );
        }
        else
        {
            Ray up_ray = new Ray( transform.position, Vector3.down );
            if( Ground.Raycast( up_ray, out hit, 100 ) )
            {
                Vector3 pos = transform.position;
                pos.y = hit.point.y + offset;
                transform.position = pos;
            }
        }

        for( int i = 0; i < bones.Count; i++ )
            bones[i].transform.localPosition = bone_positions[i];
    }

    private void LateUpdate()
    {
        if( StickBones )
        {
            RaycastHit hit;
            int i = 0;
            foreach( Transform bone in bones )
            {
                Ray bone_ray = new Ray( bone.position, Vector3.down );

                if( Ground.Raycast( bone_ray, out hit, 100 ) )
                {
                    bone_position_queues[i].Enqueue( hit.point.y + offset / 2.0f );
                    if( bone_position_queues[i].Count > BonesSmoothing )
                        bone_position_queues[i].Dequeue();

                    float sum = 0;

                    foreach( float v in bone_position_queues[i] )
                        sum += v;

                    Vector3 pos = bone.position;
                    pos.y = sum / bone_position_queues[i].Count;
                    bone.position = pos;
                }

                i++;
            }
        }
    }
}
