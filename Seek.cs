﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof( BasicMovement ))]
public class Seek : MovementAffector
{
    public Transform Target;

    private void Update()
    {
        if( Target == null )
        {
            velocity = Vector3.zero;
            return;
        }
        velocity = Target.position - transform.position;
    }
}
