﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof( BasicMovement ))]
public class Wander : MovementAffector
{
    public float MaxAngle = 10;
    public float Frequency = 1;
    
    private float time = 0;

    private Vector3 target_vel;
    
    public void Update()
    {
        time += Time.deltaTime;
        if( time >= Frequency )
        {
            Quaternion rotation = Quaternion.Euler( Random.Range( -MaxAngle, MaxAngle ), Random.Range( -MaxAngle, MaxAngle ), 0 );
            target_vel = rotation * movement.Velocity;
            time = 0;
        }

        velocity = target_vel;
        Debug.DrawRay( transform.position, velocity, Color.yellow );
    }
}
