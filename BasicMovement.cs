﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BasicMovement : MonoBehaviour
{
    [SerializeField]
    private float Speed = 1;
    public float MaxSpeed = 1;
    [SerializeField]
    private float MaxAcceleration = 0.1f;
    [SerializeField]
    private float AngularSpeed = 10;

    public Vector3 Velocity
    {
        get;
        private set;
    }
    
    private Vector3 target_velocity;

    private void Start()
    {
        Velocity = Random.insideUnitSphere;
    }

    private void UpdateTargetVelocity()
    {
        target_velocity = Vector3.zero;
        foreach( MovementAffector affector in GetComponentsInChildren<MovementAffector>() )
        {
            if( !affector.enabled )
                continue;
            target_velocity += affector.GetVelocity();
        }
        target_velocity = target_velocity.normalized;
        if( target_velocity.magnitude == 0 )
            target_velocity = Random.insideUnitSphere;
    }

    private void Update()
    {
        Speed += MaxAcceleration;
        
        if( Speed > MaxSpeed )
            Speed = MaxSpeed;
        
        UpdateTargetVelocity();

        float angle = Vector3.Angle( Velocity.normalized, target_velocity.normalized ) / 90;

        float angle_speed = AngularSpeed * angle;
        
        Velocity = Vector3.RotateTowards( Velocity.normalized, target_velocity.normalized,
                                          angle_speed * Time.deltaTime, Speed * Time.deltaTime );
        
        transform.position += Velocity.normalized * Speed * Time.deltaTime;
        Debug.DrawRay( transform.position, Velocity.normalized * 2, Color.green );
    }
}
